import os
import numpy as np
from nose.tools import raises

from greendale import louvain
thisdir = os.path.dirname(os.path.realpath(__file__))


def test_blockdiag():
    A = np.loadtxt(os.path.join(thisdir, 'blockdiag.txt'))
    cmap = louvain.single_pass(A, gamma=1.0)
    assert len(set(cmap[0:5])) == 1
    assert len(set(cmap[5:15])) == 1
    assert len(set(cmap[15:35])) == 1
    assert len(set(cmap[35:45])) == 1
    assert len(set(cmap[45:50])) == 1
    assert len(set(cmap)) == 5
