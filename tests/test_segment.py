import os
import numpy as np
from nose.tools import raises

from greendale import segment
thisdir = os.path.dirname(os.path.realpath(__file__))



def test_potts_zeros():
    A = np.zeros((20,20))
    k = A.sum(axis=0)
    #Wcomm = segment.weights_by_segment(A) #divides by zero!
    #Wnull = segment.weights_by_segment(np.outer(k,k)) #divides by zero!
    Wcomm = A.copy()
    Wnull = A.copy()
    starts, scores = segment.potts_segmentation(Wcomm, Wnull, gamma=1.0)
    assert np.all(starts == [0])
    starts, scores = segment.potts_segmentation(Wcomm, Wnull, gamma=2.0)
    assert np.all(starts == [0])

def test_potts_ones():
    A = np.ones((20,20))
    k = A.sum(axis=0)
    Wcomm = segment.normalized_weights_by_segment(A)
    Wnull = segment.normalized_weights_by_segment(np.outer(k,k))
    starts, scores = segment.potts_segmentation(Wcomm, Wnull, gamma=1.0)
    assert np.all(starts == [0])
    starts, scores = segment.potts_segmentation(Wcomm, Wnull, gamma=2.0)
    assert np.all(starts == np.arange(20))

def test_potts_blockdiag():
    A = np.loadtxt(os.path.join(thisdir, 'blockdiag.txt')) # 50x50
    A -= np.eye(len(A))
    k = A.sum(axis=0)
    Wcomm = segment.normalized_weights_by_segment(A)
    Wnull = segment.normalized_weights_by_segment(np.outer(k,k))
    starts, scores = segment.potts_segmentation(Wcomm, Wnull, gamma=1.0)
    assert np.all(starts == [0, 5, 15, 35, 45])



def test_armatus_zeros():
    A = np.zeros((20,20))
    k = A.sum(axis=0)
    Wcomm = segment.weights_by_segment(A)
    starts, scores = segment.armatus_segmentation(Wcomm, gamma=1.0)
    assert np.all(starts == [0])

# def test_armatus_ones():
#     A = np.ones((20,20))
#     k = A.sum(axis=0)
#     Wcomm = segment.normalized_weights_by_segment(A)
#     starts, scores = segment.armatus_segmentation(Wcomm, gamma=0)
#     assert np.all(starts == [0])

def test_armatus_blockdiag():
    A = np.loadtxt(os.path.join(thisdir, 'blockdiag.txt'))
    A -= np.eye(len(A))
    Wcomm = segment.normalized_weights_by_segment(A)
    starts, scores = segment.armatus_segmentation(Wcomm, gamma=1.0)
    assert np.all(starts == [0, 5, 15, 35, 45])
