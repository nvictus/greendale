from __future__ import division
import collections
import itertools

import numpy as np
where = np.flatnonzero

from ..utils import mask_compress, mask_restore_starts, spans2cmap
from .weightcalc import weights_by_segment, normalized_weights_by_segment, means_by_diagonal
from . import dp, mcmc


class Segmenter(object):
    def __init__(self, num_nodes, scoring_func, *args, **kwargs):
        self.num_nodes = num_nodes
        self.scoring_func_factory = partial(scoring_func, *args, **kwargs)

    def run(self, *args):
        f = self.scoring_func_factory(*args)
        scores, optk = dp.solve_dp(self.num_nodes, f)
        starts = dp.get_starts(scores, optk)
        return starts, scores

def potts_segmentation(Wcomm, Wnull, gamma, pass_mask=None):
    """
    Returns:
        starts : ordered list of start boundaries of domains
        scores : history of DP score (n iterations)

    """
    if pass_mask is not None:
        Wcomm = mask_compress(Wcomm, pass_mask)
        Wnull = mask_compress(Wnull, pass_mask)

    f = dp.PottsScoringFunction(Wcomm, Wnull, gamma)
    opt, optk = dp.solve_dp(len(Wcomm), f)
    starts = dp.get_starts(opt, optk)

    if pass_mask is not None:
        starts = mask_restore_starts(starts, pass_mask)

    return starts, opt


def armatus_segmentation(Wcomm, gamma, pass_mask=None):
    """
    Returns:
        starts : ordered list of start boundaries of domains
        scores : history of DP score (n iterations)
    
    """
    if pass_mask is not None:
        Wcomm = mask_compress(Wcomm, pass_mask)

    f = dp.ArmatusScoringFunction(Wcomm, gamma)
    opt, optk = dp.solve_dp(len(Wcomm), f)
    starts = dp.get_starts(opt, optk)

    if pass_mask is not None:
        starts = mask_restore_starts(starts, pass_mask)

    return starts, opt


def consensus(seg_func, gammas, minsize=3, cutoff=0):
    """
    Input:
        seg_func: a "partial function" closure over one of the segmentation 
                  functions, e.g.:
                    functools.partial(potts_segmentation, A, pass_mask=m)
        gammas: a sequence of resolution parameters
        minsize: (size filter) min segment size
        cutoff: (density filter) min fraction of total edge density within 
                community
    Returns:
        list of tuples (consensus non-overlapping domains)

    """
    domains = []
    for gamma in gammas:
        pos, _ = seg_func(gamma)
        domains.extend(zip(pos[:-1], pos[1:]))
    domains.sort()
    domains = [d for d in domains if d[1]-d[0]>minsize]
    domains = [d for d in domains \
        if W_inner[d[0], d[1]-1] > cutoff*(W_total[d[0], d[1]-1] - W_inner[d[0], d[1]-1])]
    occ = collections.Counter(domains)
    return dp.consensus_domains(domains, occ)


def potts_mc(num_sweeps, W_inner, W_total, gamma, m, T=1.0, initial_state=None, 
             pass_mask=None):
    if pass_mask is None:
        positions = np.arange(len(W_inner), dtype=int)
    else:
        positions = where(pass_mask)

    N = len(W_inner)
    beta = 1/T
     
    if initial_state is None:
        # start with one community (whole graph)
        initial_state = np.zeros(N, dtype=int)
        initial_state[0] = 1
    else:
        initial_state = np.asarray(initial_state, dtype=int)

    # state = np.ones(N, dtype=int) 
    # state = np.random.choice([0,1], size=N, replace=True)
    # initial score
    # q0 = W_inner[0,N-1] - gamma*W_total[0,N-1]**2

    # do sampling
    states, events, gains = _mcmc.potts_metropolis(
        initial_state, positions, W_inner, W_total**2, m, beta, gamma, num_sweeps)

    return states, events, gains


def armatus_mc(num_sweeps, W_inner, gamma, m, T=1.0, initial_state=None, 
               pass_mask=None):
    if pass_mask is None:
        positions = np.arange(len(W_inner), dtype=int)
    else:
        positions = where(pass_mask)

    N = len(W_inner)
    beta = 1/T
    mu = compute_mean_scaled_weights_by_size(W_inner)

    if initial_state is None:
        initial_state = np.zeros(N, dtype=int)
        initial_state[0] = 1
    else:
        initial_state = np.asarray(initial_state, dtype=int)

    states, events, gains = _mcmc.armatus_metropolis(
        initial_state, positions, W_inner, mu, m, beta, gamma, num_sweeps)

    return states, events, gains


def top_solutions(K, scorer, opt):
    import pqdict
    N = len(opt) - 1
    top = np.zeros((N+1,K), dtype=float)
    topk = np.zeros((N+1,K), dtype=int)

    top[:,0] = opt
    top[0,1:] = -np.inf
    top[1,1:] = -np.inf
    for i in range(1, N+1):
        heap = pqdict.PQDict.maxpq()
        for k in range(0, i):
            heap[k] = (top[k,0] + scorer.eval(k,i-1), 0)

        for rank in range(K):
            k, (score, r) = heap.topitem()
            r = -r
            if r+1 < K:
                heap.updateitem(k, (top[k, r+1] + scorer.eval(k,i-1), -(r+1)))
            top[i, rank] = score
            topk[i, rank] = k

    return top, topk


def top_solutions_with_gap(K, scorer, opt, optD, optG):
    import pqdict
    N = len(opt) - 1
    top = np.zeros( (N+1,K), dtype=float)
    topk = np.zeros( (N+1,K), dtype=int)
    topD = np.zeros( (N+1,K), dtype=float)
    topkD = np.zeros( (N+1,K), dtype=int)
    topG = np.zeros( (N+1,K), dtype=float)
    topkG = np.zeros( (N+1,K), dtype=int)
    gap = np.zeros( (N+1,K), dtype=int)

    topD[:,0] = optD
    topG[:,0] = optG
    top[:,0] = opt
    for r in range(K):
        top[r,r+1:] = -np.inf
    for i in range(1, N+1):
        heapD = pqdict.PQDict.maxpq()
        heapG = pqdict.PQDict.maxpq()
        for k in range(0, i):
            heapD[k] = (top[k,0] + scorer.eval(k,i-1), 0)
            heapG[k] = (topD[k,0], 0)

        prev = 0
        for rank in range(K):
            k, (score, r) = heapD.topitem()
            r = -r
            topD[i, rank] = score
            topkD[i, rank] = k
            if r+1 < K:
                heapD.updateitem(k, (top[k, r+1] + scorer.eval(k,i-1), -(r+1)))

            k, (score, r) = heapG.topitem()
            r = -r
            topG[i, rank] = score
            topkG[i, rank] = k
            if r+1 < K:
                heapG.updateitem(k, (topD[k, r+1],-(r+1)))

            if topG[i, rank] > topD[i, prev]:
                top[i, rank] = topG[i, rank]
                topk[i, rank] = topkG[i, rank]
                gap[i, rank] = 1
            else:
                top[i, rank] = topD[i, prev]
                topk[i, rank] = topkD[i, prev]
                gap[i, rank] = 0
                prev += 1

    return top, topk, gap

