import cython
import numpy as np
cimport numpy as np
from libc.math cimport log

def potts_metropolis(np.ndarray[np.int_t, ndim=1] initial_state,
           np.ndarray[np.int_t, ndim=1] positions,
           np.ndarray[np.double_t, ndim=2] Wins, 
           np.ndarray[np.double_t, ndim=2] Wtot2, 
           double m, 
           double beta, 
           double gamma,
           int num_sweeps):

    cdef int N = initial_state.size
    cdef int M = positions.size
    cdef np.ndarray[np.int_t, ndim=2] state = np.zeros((num_sweeps,N), dtype=int)
    cdef np.ndarray[np.int_t, ndim=2] events = np.zeros((num_sweeps,M), dtype=int)
    cdef np.ndarray[np.double_t, ndim=2] gains = np.zeros((num_sweeps,M), dtype=float)
    cdef np.ndarray[np.int_t, ndim=1] random_ints = np.random.random_integers(1,M-1, size=(M,)) 
    state[0,:] = initial_state

    cdef int s
    for s in range(1,num_sweeps):
        potts_sweep(s, M, state, events, gains, positions, random_ints, Wins, Wtot2, m, beta, gamma)

    return state, events, gains

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef potts_sweep(int s,
          int M,
          np.ndarray[np.int_t, ndim=2] state,
          np.ndarray[np.int_t, ndim=2] events,
          np.ndarray[np.double_t, ndim=2] gains,
          np.ndarray[np.int_t, ndim=1] positions,
          np.ndarray[np.int_t, ndim=1] random_ints,
          np.ndarray[np.double_t, ndim=2] Wins,
          np.ndarray[np.double_t, ndim=2] Wtot2,
          double m,
          double beta,
          double gamma):

    cdef int i, j, k, here, left, right
    cdef double W1, W2, W12
    cdef double gain, loglik

    # do a sweep of M iterations
    for k in range(0, M):

        # pick a random site (never the first)
        i = random_ints[k]
        here = positions[i]
        if i == 1:
            left = positions[0]
        else:
            for j in range(i-1, 0, -1):
                left = positions[j]
                if state[s,left] == 1:
                    break
        if i == M-1:
            right = positions[M-1]+1
        else:
            for j in range(i+1, len(positions)):
                right = positions[j]
                if state[s,right] == 1:
                    break

        # propose to delete a boundary (i.e. merge two communities)
        # or create a boundary (i.e. split a community)
        W1  = Wins[left, here-1] - gamma*Wtot2[left, here-1]
        W2  = Wins[here, right-1] - gamma*Wtot2[here, right-1]
        W12 = Wins[left, right-1] - gamma*Wtot2[left, right-1]
        if state[s,here] == 1: 
            gain = W12 - W1 - W2
        else: 
            gain = W1 + W2 - W12

        # acceptance probability (deltaE = -m*deltaQ, loglik = -beta*deltaE)
        loglik = beta*m*gain

        # decide whether to toggle the boundary or not (Metropolis)
        if gain >= 0 or log(np.random.rand()) < loglik:
            state[s,here] = 0 if state[s,here] == 1 else 1
            events[s,k] = i
            gains[s,k] = gain
        else:
            events[s,k] = 0
            gains[s,k] = 0.0


def armatus_metropolis(np.ndarray[np.int_t, ndim=1] initial_state,
           np.ndarray[np.int_t, ndim=1] positions,
           np.ndarray[np.double_t, ndim=2] Wins, 
           np.ndarray[np.double_t, ndim=1] mu, 
           double m, 
           double beta, 
           double gamma,
           int num_sweeps):

    cdef int N = initial_state.size
    cdef int M = positions.size
    cdef np.ndarray[np.int_t, ndim=2] state = np.zeros((num_sweeps,N), dtype=int)
    cdef np.ndarray[np.int_t, ndim=2] events = np.zeros((num_sweeps,M), dtype=int)
    cdef np.ndarray[np.double_t, ndim=2] gains = np.zeros((num_sweeps,M), dtype=float)
    cdef np.ndarray[np.int_t, ndim=1] random_ints = np.random.random_integers(1,M-1, size=(M,)) 
    state[0,:] = initial_state

    cdef int s
    for s in range(1,num_sweeps):
        armatus_sweep(s, M, state, events, gains, positions, random_ints, Wins, mu, m, beta, gamma)

    return state, events, gains

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef armatus_sweep(int s,
          int M,
          np.ndarray[np.int_t, ndim=2] state,
          np.ndarray[np.int_t, ndim=2] events,
          np.ndarray[np.double_t, ndim=2] gains,
          np.ndarray[np.int_t, ndim=1] positions,
          np.ndarray[np.int_t, ndim=1] random_ints,
          np.ndarray[np.double_t, ndim=2] W,
          np.ndarray[np.double_t, ndim=1] mu,
          double m,
          double beta,
          double gamma):

    cdef int i, j, k, here, left, right
    cdef double W1, W2, W12
    cdef double gain, loglik

    # do a sweep of M iterations
    for k in range(0, M):

        # pick a random site (never the first)
        i = random_ints[k]
        here = positions[i]
        if i == 1:
            left = positions[0]
        else:
            for j in range(i-1, 0, -1):
                left = positions[j]
                if state[s,left] == 1:
                    break
        if i == M-1:
            right = positions[M-1]+1
        else:
            for j in range(i+1, len(positions)):
                right = positions[j]
                if state[s,right] == 1:
                    break

        # propose to delete a boundary (i.e. merge two communities)
        # or create a boundary (i.e. split a community)
        W1  = W[left, i-1]/(i-left)**gamma - mu[i-left]
        W2  = W[i, right-1]/(right-i)**gamma - mu[right-i]
        W12 = W[left, right-1]/(right-left)**gamma - mu[right-left]
        if state[s,here] == 1: 
            gain = W12 - W1 - W2
        else: 
            gain = W1 + W2 - W12

        # acceptance probability (deltaE = -m*deltaQ, loglik = -beta*deltaE)
        loglik = beta*m*gain

        # decide whether to toggle the boundary or not (Metropolis)
        if gain >= 0 or log(np.random.rand()) < loglik:
            state[s,here] = 0 if state[s,here] == 1 else 1
            events[s,k] = i
            gains[s,k] = gain
        else:
            events[s,k] = 0
            gains[s,k] = 0.0









