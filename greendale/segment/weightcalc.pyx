import cython
import numpy as np
cimport numpy as np


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef weights_by_segment(np.ndarray[np.double_t, ndim=2] A):
    """
    Aggregate pairwise edge weights for every segment.

    Input:
        A symmetric square matrix of pairwise weights between nodes. A[i,j] is 
        the weight corresponding to nodes i and j.
        If there are self-weights, the values on the diagonal A[i,i] are 
        assumed to be twice the weight.

    Output:
        A symmetric square matrix of summed weights for every contiguous segment 
        of nodes. W[a,b] is the sum of weights for all pairs in the closed range 
        [a..b] (including b).

    """
    cdef int N = len(A)
    cdef np.ndarray[np.double_t, ndim=2] W = np.zeros((N,N), dtype=float)

    cdef int i
    for i in range(N):
        W[i,i] = A[i,i]/2.0

    cdef int start, end
    cdef np.ndarray[np.double_t, ndim=1] cumsum
    for end in range(1, N):
        cumsum = np.zeros(end+1, dtype=float)
        cumsum[end] = A[end,end]

        for start in range(end-1, -1, -1):
            cumsum[start] = cumsum[start+1] + A[start,end]
            W[start,end] = W[end,start] = W[start, end-1] + cumsum[start]

    return W


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef normalized_weights_by_segment(np.ndarray[np.double_t, ndim=2] A):
    """
    Aggregate pairwise edge weights for every segment and normalize by the 
    total weight of the graph. If there are self-weights, the values on the 
    diagonal A[i,i] are assumed to be twice the edge weight.

    """
    cdef np.ndarray[np.double_t, ndim=1] deg = A.sum(axis=0)
    cdef double m = deg.sum()/2.0
    cdef np.ndarray[np.double_t, ndim=2] W = weights_by_segment(A)
    W /= m
    return W


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef mean_scaled_weights_by_size(np.ndarray[np.double_t, ndim=2] W, double gamma):
    cdef int N = len(W)
    cdef np.ndarray[np.double_t, ndim=1] mu = np.zeros(N+1, dtype=float)
    cdef np.ndarray[np.int_t, ndim=1] count = np.zeros(N+1, dtype=int)

    cdef int i
    for i in range(N):
        mu[1] = (count[1]*mu[1] + W[i,i])/(count[1] + 1)
        count[1] += 1

    cdef int start, end, size
    cdef double r
    for end in xrange(1, N):
        for start in xrange(end-1, -1, -1):
            size = end - start + 1  #closed interval (includes end)
            r = W[start, end] / size**gamma
            mu[size] = (count[size]*mu[size] + r)/(count[size] + 1)
            count[size] += 1
    return mu


def logbins(a, b, r=None, N=None):
    a, b = int(a), int(b)
    a10, b10 = np.log10([a, b])
    if r is not None:
        N = np.ceil(np.log(b/a) / np.log(r))
    elif N is None:
        raise TypeError
    starts = np.rint(np.logspace(a10, b10, N)).astype(int)
    return np.unique(starts)


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cpdef means_by_diagonal(np.ndarray[np.double_t, ndim=2] A):
    """
    Diagonals are grouped into bins and the elements in each bin are averaged.
    The bins increase in size geometrically.

    Input:
        Symmetric square matrix A

    Output: 1D array
        dmeans[s] is the average value of the bin containing the sth diagonal.

    """
    cdef int N = len(A)
    _bins = logbins(1,N, 1.05)
    _bins = [(0,1)] + [(_bins[i],_bins[i+1]) for i in range(len(_bins)-1)]
    _bins = np.array(_bins, dtype=int)
    cdef np.ndarray[np.int_t, ndim=2] bins = _bins
    
    cdef np.ndarray[np.double_t, ndim=1] dmeans = np.zeros(N, dtype=float)
    cdef int M = len(bins)
    cdef double binsum
    cdef int count
    cdef int d, j, k
    cdef int start, stop
    for k in range(M):
        start = bins[k,0]
        stop  = bins[k,1]
        binsum = 0.0
        count = 0
        for d in range(start, stop):
            for j in range(0, N-d):
                binsum += A[d+j, j]
                count += 1
        dmeans[start:stop] = binsum/count
    return dmeans
