import cython
import numpy as np
cimport numpy as np
#from libc.math cimport max

from .weightcalc import mean_scaled_weights_by_size


cdef class ScoringFunction:
    cpdef double eval(self, int k, int i):
        return 0


cdef class PottsScoringFunction(ScoringFunction):
    cdef double [:, :] Q
    def __init__(self, 
                 np.ndarray[np.double_t, ndim=2] Wcomm, 
                 np.ndarray[np.double_t, ndim=2] Wnull, 
                 double gamma):
        self.Q = Wcomm - gamma*Wnull
    def __dealloc__(self):
        self.Q = None
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    cpdef double eval(self, int k, int i):
        return self.Q[k,i]


cdef class ArmatusScoringFunction(ScoringFunction):
    cdef double [:, :] W
    cdef double [:] mu
    cdef double gamma
    def __init__(self, np.ndarray[np.double_t, ndim=2] Wcomm, double gamma):
        self.W = Wcomm
        self.gamma = gamma
        self.mu = mean_scaled_weights_by_size(Wcomm, gamma)
    def __dealloc__(self):
        self.W = None
        self.mu = None
    @cython.cdivision(True)
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    cpdef double eval(self, int k, int i):
        cdef int size = i - k + 1
        return max(self.W[k,i]/size**self.gamma - self.mu[size], 0)


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def solve_dp(int num_nodes, ScoringFunction scorer):
    cdef int N = num_nodes
    cdef np.ndarray[np.double_t, ndim=1] opt = np.zeros(N+1, dtype=float)
    cdef np.ndarray[np.int_t, ndim=1] optk = np.zeros(N+1, dtype=int)

    cdef int i, k
    cdef double s
    opt[0] = 0.0
    for i in range(1, N+1):
        opt[i] = -np.inf
        optk[i] = i-1
        for k in range(0, i):
            s = opt[k] + scorer.eval(k, i-1)
            if s > opt[i]:
                opt[i] = s
                optk[i] = k

    return opt, optk


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def get_starts(np.ndarray[np.double_t, ndim=1] opt, np.ndarray[np.int_t, ndim=1] optk):
    cdef int N = len(opt) - 1
    cdef np.ndarray[np.int_t, ndim=1] starts = np.zeros(N, dtype=int)
    cdef int j = 0
    i = N
    while i > 0:
        i = starts[j] = optk[i]
        j += 1

    return starts[:j][::-1]


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
def solve_dp_with_gaps(int num_nodes, ScoringFunction scorer):
    cdef int N = num_nodes
    cdef np.ndarray[np.double_t, ndim=1] optG = np.zeros(N+1, dtype=float)
    cdef np.ndarray[np.double_t, ndim=1] optD = np.zeros(N+1, dtype=float)
    cdef np.ndarray[np.double_t, ndim=1] opt  = np.zeros(N+1, dtype=float)
    cdef np.ndarray[np.int_t, ndim=1] optkG = np.zeros(N+1, dtype=int)
    cdef np.ndarray[np.int_t, ndim=1] optkD = np.zeros(N+1, dtype=int)
    cdef np.ndarray[np.int_t, ndim=1] optk  = np.zeros(N+1, dtype=int)
    cdef np.ndarray[np.int_t, ndim=1] gap  = np.zeros(N+1, dtype=int)

    cdef int i, k
    cdef double s, score
    optG[0] = 0.0 #best solution that ends with a gap
    optD[0] = 0.0 #best solution that ends with a domain
    opt[0]  = 0.0 #best overall solution
    for i in range(1, N+1):
        # 1. Solve G(i)
        # G(i) = max_{k<i} D(k)
        # find k with best score ending in gap from [k..i-1]
        optG[i] = -np.inf
        optkG[i] = i-1
        for k in range(0, i):
            s = optD[k]
            if s > optG[i]:
                optG[i] = s
                optkG[i] = k

        # 2. Solve D(i)
        # D(i) = max_{k<i} OPT(k) + q(k,i-1)
        # find k with best score ending in a segment from [k..i-1]
        optD[i] = -np.inf
        optkD[i] = i-1
        for k in range(0, i):
            s = opt[k] + scorer.eval(k, i-1)
            if s > optD[i]:
                optD[i] = s
                optkD[i] = k

        # 3. Choose the better solution for OPT(i)
        # OPT(i) = max(G(i), D(i))
        if optG[i] > optD[i]:
            opt[i] = optG[i]
            optk[i] = optkG[i]
            gap[i] = 1
        else:
            opt[i] = optD[i]
            optk[i] = optkD[i]
            gap[i] = 0

    return opt, optk, gap, optD, optG


def consensus_domains(list domains, occ):
    # Returns consensus list of domains
    # domains are 2-tuples given as half-open intervals [a,b)
    cdef int i, j, s_choose, s_ignore
    cdef tuple d

    # map each domain to its closest non-overlapping predecessor
    cdef int M = len(domains)
    cdef np.ndarray[np.int_t, ndim=1] prev = np.zeros(M, dtype=int)
    for i in range(M-1, -1, -1):
        d = domains[i]
        j = i - 1
        while j > -1:
            if domains[j][1] <= d[0]: 
                prev[i] = j
                break
            j -= 1

    # weighted interval scheduling dynamic program
    cdef np.ndarray[np.int_t, ndim=1] score = np.zeros(M, dtype=int)
    for i in range(1, M):
        d = domains[i]
        s_choose = score[prev[i]] + occ[d]
        s_ignore = score[i-1]
        score[i] = max(s_choose, s_ignore)

    cdef list consensus = []
    j = M - 1
    while j > 0:
        if score[j] != score[j-1]:
            consensus.append(domains[j])
            j = prev[j]
        else:
            j -= 1

    return consensus[::-1]
