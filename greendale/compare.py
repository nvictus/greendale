from __future__ import division

import numpy as np
where = np.flatnonzero

def infoentropy(x):
    N = len(x)
    H = 0
    for k in np.unique(x):
        pk = (x == k).sum()/N
        H -= pk*np.log(pk)
    return H

def mutualinfo(x, y):
    N = len(x)
    ux = np.unique(x)
    uy = np.unique(y)
    I = 0
    for i in xrange(len(ux)):
        for j in xrange(len(uy)):
            pij = ((x == ux[i]) & (y == uy[j])).sum()/N
            pi = (x == ux[i]).sum()/N
            pj = (y == uy[j]).sum()/N
            if pij > 0:
                I += pij*np.log(pij/(pi*pj))
    return I

def varinfo(x, y):
    Hx = infoentropy(x)
    Hy = infoentropy(y)
    Ixy = mutualinfo(x, y)
    return Hx + Hy - 2*Ixy