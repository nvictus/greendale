from __future__ import division, print_function
import itertools
import logging

import numpy as np
where = np.flatnonzero


def _modularity(A, m2, gamma, mmap):
    q = 0
    for members in mmap:
        W2ins = A[members,:][:,members].sum()
        W2tot = A[members,:].sum()
        if W2tot > 0:
            q += W2ins/m2 - gamma*(W2tot/m2)**2
    return q


def _pass(A, gamma, use_self=False, adj=None, cmap0=None, shuffle=False,
          verbose=False):
    if not use_self:
        A[np.diag_indices_from(A)] = 0

    if adj is None:
        adj = [where(row) for row in A]

    N = len(A)
    deg = A.sum(axis=0)  # 2 x sum of weights incident to node (node degree)
    m2 = deg.sum()       # 2 x total edge weight

    # W2tot -> 2 x sum of weights incident to nodes in each community
    # W2ins -> 2 x sum of weights inside each community
    if cmap0 is not None:
        cmap = np.asarray(cmap0, dtype=int)
        weight = {c: A[where(cmap == c),:].sum() for c in set(cmap)}
        W2tot = np.array(weight[cmap[i]] for i in xrange(N))
    else:
        cmap = np.arange(N, dtype=int)
        W2tot = deg.copy()
        #W2ins = 2.0*A.diagonal().copy()

    # list of all nodes (order affects the algorithm)
    nodes = np.arange(N)
    if shuffle:
        np.random.shuffle(nodes)
        cmap = cmap[nodes]
        W2tot = W2tot[nodes]

    if N == 1 or m2 == 0:
        if verbose: print("Nothing to do!")
        return None

    for niter in itertools.count(1):
        anything_moved = False
        if verbose:
            print("Iteration {}".format(niter))

        # Go over all nodes, attempting to move them to neighboring groups
        for node in nodes:
            # remove node from its community
            cnode = cmap[node]
            W2tot[cnode] -= deg[node]
            #W2ins[cnode] -= 2.0*A[node, cmap==cnode].sum()
            cmap[node] = -1

            # look for the best neighboring community
            best_comm = cnode
            best_gain = -float('inf')
            seen = set()
            for neighbor in adj[node]:
                c = cmap[neighbor]
                if c not in seen:
                    seen.add(c)
                    W2nc = 2.0*A[node, cmap==c].sum()
                    W2n = deg[node]
                    gain = W2nc/m2 - gamma * (W2tot[c]/m2) * (W2n/m2)
                    if gain > best_gain:
                        best_comm = c
                        best_gain = gain

            cnew = cnode
            if best_comm != cnode and best_gain > 0:
                cnew = best_comm
                anything_moved = True
                if verbose:
                    print("\tMoved node {0} to node {1}'s community: "
                          "gain = {2}".format(node, cnew, best_gain))

            # add node to its new community
            cmap[node] = cnew
            #W2ins[cnew] += 2.0*A[node, cmap==cnew].sum()
            W2tot[cnew] += deg[node]

        if not anything_moved:
            break

    print("Pass took {} iterations".format(niter))
    return cmap


def qscore(A, gamma, cmap):
    m2 = A.sum()
    return _modularity(A, m2, gamma,
        [where(cmap == c) for c in set(cmap) if c > -1])


def aggregate_adj_matrix(A, mmap):
    k = len(mmap)
    K = np.zeros((k, k))
    for i in xrange(k):
        for j in xrange(i, k):
            if i == j:
                members = mmap[i]
                K[i,i] = A[members,:][:,members].sum()
            else:
                i_members = mmap[i]
                j_members = mmap[j]
                K[i,j] = K[j,i] = A[i_members,:][:,j_members].sum()
    return K


def single_pass(A, gamma, **kwargs):
    cmap = _pass(A, gamma, **kwargs)
    if cmap is None:
        raise ValueError("No further partition is possible")
    # relabel communities by descending size rank
    mmap = sorted(
        [where(cmap == c) for c in set(cmap)],
        key=len,
        reverse=True)
    for i, members in enumerate(mmap):
        cmap[members] = i
    return cmap


def repass(A, cmap, gamma, **kwargs):
    cmap = np.array(cmap)
    mmap = [where(cmap == c) for c in set(cmap)]
    Anext = aggregate_adj_matrix(A, mmap)

    # do next-level pass
    cmap_next = _pass(Anext, gamma, **kwargs)
    if cmap_next is None:
        raise ValueError("No further partition is possible")
    next_comms = set(cmap_next)
    mmap_next = sorted(
        [where(cmap_next == c) for c in next_comms],
        key=len,
        reverse=True)
    # Update node assignments to the next-level labels.
    for i, members in enumerate(mmap):
        cmap[members] = cmap_next[i]

    # Sort next-level communities by descending size rank.
    # Relabel node assignments according to sort order
    mmap = [where(cmap == c) for c in next_comms]
    mmap.sort(key=len, reverse=True)
    for i, members in enumerate(mmap):
        cmap[members] = i
    return cmap
