import sys
import argparse
import numpy as np

from . import segment
from . import louvain
from . import spectral
from .utils import *

def main():
    pass

# def run_cli(matrix_file, format, out, gamma, multilevel, verbose):
#     if format is None:
#         A = np.loadtxt(matrix_file)
#     else:
#         if matrix_file.endswith('.mat'):
#             from scipy.io import loadmat
#             A = loadmat(matrix_file)['A']
#         elif matrix_file.endswith('.npy'):
#             A = np.load(matrix_file)
#         else:
#             A = np.loadtxt(matrix_file)

#     mask = A.sum(axis=0) != 0
#     A = mask_filter(A, pass_mask=mask)
    
#     output = communitize(A, gamma=gamma, multilevel=multilevel, verbose=verbose)

#     if multilevel:
#         for i, odict in enumerate(output):
#             cmap = mask_restore(odict['cmap'], mask, fillvalue=-1)
#             np.savetxt('{}-{}.txt'.format(out, i), cmap, fmt='%d')
#     else:
#         cmap = mask_restore(output['cmap'], mask, fillvalue=-1)
#         np.savetxt('{}.txt'.format(out), cmap, fmt='%d')
#     return 0

# def main():
#     p = argparse.ArgumentParser()

#     # required
#     p.add_argument('matrix_file', type=str,
#         help="file containing adjacency matrix")

#     # options
#     p.add_argument('-f', '--format', type=str, default=None,
#         help="input file format [txt, mat, npy] (default: inferred from extension)")
#     p.add_argument('-o', '--out', type=str, default='out',
#         help="output file basename (default: out)")
#     p.add_argument('-g', '--gamma', type=int, default=1,
#         help="resolution parameter (default: 1)")
#     p.add_argument('-m', '--multilevel', action='store_true', default=False,
#         help="apply louvain clustering hierarchically")
#     p.add_argument('-v', '--verbose', type=bool, default=False,
#         help="print lots of info")

#     args = vars(p.parse_args(sys.argv[1:]))
#     return run_cli(**args)

# def run_cli(matrix_file, format, out, method, gamma):
#     if format is None:
#         A = np.loadtxt(matrix_file)
#     else:
#         if matrix_file.endswith('.mat'):
#             from scipy.io import loadmat
#             A = loadmat(matrix_file)['A']
#         elif matrix_file.endswith('.npy'):
#             A = np.load(matrix_file)
#         else:
#             A = np.loadtxt(matrix_file)

#     num_nodes = len(A)
#     pass_mask = A.sum(axis=0) != 0
#     Wins, Wtot = compute_segment_weights(A)
#     m = method.lower()
#     if m=='potts':
#         starts, score = potts_segmentation(Wins, Wtot, gamma=gamma, pass_mask=pass_mask)
#     elif m=='armatus':
#         starts, score = armatus_segmentation(Wins, gamma=gamma, pass_mask=pass_mask)
#     pos = starts + [num_nodes]
#     dom = zip(pos[:-1], pos[1:])
#     dom = [d for d in dom if Wins[d[0], d[1]-1] > 0]
#     cmap = intervals2cmap(num_nodes, dom)
#     np.savetxt('{}.txt'.format(out), cmap, fmt='%d')
#     return 0

# def main():
#     p = argparse.ArgumentParser()

#     # required
#     p.add_argument('matrix_file', type=str,
#         help="file containing adjacency matrix")

#     # options
#     p.add_argument('-f', '--format', type=str, default=None,
#         help="input file format [txt, mat, npy] (default: inferred from extension)")
#     p.add_argument('-o', '--out', type=str, default='out',
#         help="output file basename (default: out)")
#     p.add_argument('-g', '--gamma', type=int, default=1,
#         help="resolution parameter (default: 1)")
#     p.add_argument('-m', '--method', type=str, default='potts',
#         help="objective function (potts or armatus)")

#     args = vars(p.parse_args(sys.argv[1:]))
#     return run_cli(**args)