from __future__ import division
from functools import wraps
import itertools
import math

from scipy.linalg import eig as _eig, eigh as _eigh, svd as _svd
from scipy.sparse.linalg import eigs as _eigs, eigsh as _eigsh
import numpy as np

from ..utils import mask_compress, mask_restore

def std_estimate(A):
    Aflat = A.flat
    if len(Aflat) < 5:
        return A.mean()
    if len(Aflat) < 10000:
        return A.var()
    var = 0.25*(Aflat[::100].var() + Aflat[33::231].var() +\
        Aflat[24::242].var() + Aflat[55::413].var())
    return np.sqrt(var)

def issymmetric(A):
    maxdiff = np.abs(A - A.T).max()
    return maxdiff < 0.00001 + 0.0000001*std_estimate(A)
    # (np.abs(A - A.T).mean()/np.abs(A.mean())) < 1e-10

def laplacian(A): #SM
    """
    Computes the graph Laplacian of graph with adjacency matrix A.

    The Laplacian is positive semidefinite, with smallest eigenvalue 0 with
    vector (1,1,1,...). 
        - The smallest non-zero eigenvalue is called the spectral gap. 
        - The second smallest non-zero eigenvalue is called the algebraic 
          connectivity or Fiedler value. 

    The Fiedler value is a lower bound on the minimum normalized cut of the 
    graph. The signs of the elements of the Fiedler eigenvector can be used to 
    bipartition a graph. 

    """
    k = A.sum(axis=0)
    return np.diag(k) - A

def nlaplacian(A):
    """
    Symmetric normalized Laplacian. Often used for spectral clustering.

    """
    k = A.sum(axis=0)
    D = np.diag(k)
    return np.eye(len(A)) - np.sqrt(D).dot(A).dot(np.sqrt(D))

def modularity(A, gamma=1): #LM
    """
    Modularity Matrix (Newman, 2006) with an additional resolution parameter, 
    gamma.

    The principal eigenvector of this matrix provides a bipartition of the 
    graph into two communities if the eigenvalue is positive. If there are no
    positive eigenvalues, the leading eigenvalue will be zero, corresponding
    to (1,1,1,...), meaning that there is no bipartition of the graph that would 
    increase the modularity.

    """
    k = A.sum(axis=0)
    return A - gamma*np.outer(k,k)

def pagerank(A, d=0.85): #LM
    """
    Input:
        A : column-stochastic matrix with no zero columns
            --> transition probabilities of random surfer clicking a link
        d : damping factor 
            --> probability that surfer gets bored and goes to a random page

    PageRank is the principal eigenvector of this matrix: the stationary
    probability distribution over pages of a randomly clicking web surfer.

    Symmetric A can be thought of as an undirected webgraph, where all page 
    links are bidirectional.

    """
    N = len(A)
    return d*A + (1-d)*np.ones(A.shape)/N


def eig(A, pass_mask=None, k=6, which='LM', centered=False):
    A = np.asarray(A, dtype=float)
    if pass_mask is not None:
        A = mask_compress(A, pass_mask)

    if centered:
        A = (A - np.mean(A.T, axis=1)).T

    if issymmetric(A):
        print "symmetric"
        lam, v = _eigsh(A, k, which=which)
    else:
        print "not symmetric"
        lam, v = _eigs(A, k, which=which)

    if pass_mask is not None:
        v = mask_restore(v, pass_mask, axis=0)

    if which.startswith('L'):
        idx = np.argsort(-lam)
        lam, v = lam[idx], v[:,idx]
    elif which.startswith('S'):
        idx = np.argsort(lam)
        lam, v = lam[idx], v[:,idx]

    return lam, v

def pca(A, **kwargs):
    A = np.asarray(A, dtype=float)
    M = (A - np.mean(A.T, axis=1)).T
    covM = np.dot(M, M.T)
    return eig(covM, **kwargs)

def bipartition(A, gamma=1, pass_mask=None):
    A = np.asarray(A, dtype=float)
    if pass_mask is not None:
        A = mask_compress(A, pass_mask)
    
    k = A.sum(axis=0)
    E = np.outer(k,k)
    B = A - gamma*E
    lam, v = _eigsh(B, which='LA')
    idx = np.argsort(-lam)
    lam, v = lam[idx], v[:,idx]
    if lam[0] <= 0:
        return None
    
    x = v[:,0]
    s = np.ones(len(x))
    s[x >= 0] = -1

    if pass_mask is not None:
        s = mask_restore(s, pass_mask, axis=0)
    return s

def partition(A, gamma, pass_mask=None, maxlevel=np.inf):
    A = np.asarray(A, dtype=float)

    if pass_mask is not None:
        A = mask_compress(A, pass_mask)

    def bipartition_recurse(A, loc, i, cmap):
        if np.floor(np.log2(i)) + 1 >= maxlevel:
            return
        k = A.sum(axis=0)
        K = np.outer(k,k)
        try:
            lam, v = _eigsh(A-gamma*K, k=3, which='LA')
        except ValueError:
            return
        idx = np.argsort(-lam)
        lam, v = lam[idx], v[:,idx]
        x = v[:,0]

        if lam[0] <= 0:
            return
        s = np.ones(len(x))
        s[x < 0] = -1

        cmap[loc[s==-1]] = 2*i + 1
        cmap[loc[s== 1]] = 2*i + 2

        A0 = A[s==-1,:][:,s==-1]
        if len(A0) >= 3:
            bipartition_recurse(A0, loc[s==-1], 2*i+1, cmap)
        A1 = A[s== 1,:][:,s== 1]
        if len(A1) >= 3:
            bipartition_recurse(A1, loc[s== 1], 2*i+2, cmap)

    n = len(A)
    cmap = np.zeros(n, dtype=int)
    loc = np.arange(n, dtype=int)
    bipartition_recurse(A, loc, 0, cmap)

    if pass_mask is not None:
        cmap = mask_restore(cmap, pass_mask, fill_value=-1, axis=0)
    return cmap





# def refine(A, comm1, comm2, objfunc):
#     """
#     Refine a bipartition by swapping nodes.
#     Objfunc: 
#         minimize edge cut
#         maximize modularity

#     """
#     edgecut = A[np.ix_(comm1, comm2)].sum()
#     Dins = np.zeros(N)
#     Dext = np.zeros(N)
#     for a in comm1:
#         Dins[a] = A[a, comm1].sum()
#         Dext[a] = A[a, comm2].sum()
#     for b in comm2:
#         Dins[b] = A[b, comm2].sum()
#         Dext[b] = A[b, comm1].sum()
#     D = Dext - Dint

#     maxgain = -1
#     an, bn = None, None
#     nodes1 = set(zip(range(len(comm1)), comm1))
#     nodes2 = set(zip(range(len(comm2)), comm2))
#     while True:
#         maxgain = 0
#         for i, a in nodes1:
#             for j, b in nodes2:
#                 gain = D[a] + D[b] - 2*A[a,b]
#                 if gain > maxgain:
#                     imax, jmax = i, j
#                     maxgain = gain
#         amax = comm1[imax]
#         bmax = comm2[jmax]
#         comm1[imax], comm2[jmax] = bmax, amax
#         nodes1.remove((imax, amax))
#         nodes2.remove((jmax, bmax))
#         for a in comm1:
#             D[a] += 2*A[a,bmax] - 2*A[a,amax]
#         for b in comm2:
#             D[b] += 2*A[b,amax] - 2*A[b,bmax]
#         edgecut -= maxgain




