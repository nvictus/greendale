from __future__ import division
from numpy.random import randint
import math
import scipy
import numpy as np
where = np.flatnonzero

__all__ = ('spans2cmap', 'mask_compress', 'mask_restore', 'mask_compress_starts', 
    'mask_restore_starts')


def applies_mask(in_args, out_args=None):
    def decorator(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            pass_mask = kwargs.pop('pass_mask', None)
            if pass_mask:
                for i in in_args:
                    args[i] = mask_compress(args[i], pass_mask)
            out = func(*args, **kwargs)
            if pass_mask:
                if out_args is None:
                    out = mask_restore(out, pass_mask)
                else:
                    for i in out_args:
                        out[i] = mask_restore(out[i], pass_mask)
            return out
        return wrapped
    return decorator


def mask_compress(a, pass_mask, axis=(0,1)):
    if a.ndim == 1:
        return a[pass_mask]
    elif a.ndim == 2:
        if axis == (0,1):
            return a.compress(pass_mask, axis=0)\
                    .compress(pass_mask, axis=1) 
                    #a[pass_mask,:][:,pass_mask]
        elif axis == 0:
            return a.compress(pass_mask, axis=0)
                    #a[pass_mask,:]
        elif axis == 1:
            return a.compress(pass_mask, axis=1)
                    #a[:,pass_mask]
        else:
            raise TypeError("axis must be 0, 1 or (0,1)")
    raise TypeError("Input array must be rank 1 or 2")


def mask_restore(b, pass_mask, fill_value=0, axis=(0,1)):
    n = len(pass_mask)
    fill_mask = np.logical_not(pass_mask)
    if b.ndim == 1:
        a = np.zeros(n, dtype=b.dtype)
        a[pass_mask] = b
        if fill_value: a[fill_mask] = fill_value
        return a
    elif b.ndim == 2:
        if axis == (0,1):
            a = np.zeros((n,n), dtype=b.dtype)
            a[np.ix_(pass_mask,pass_mask)] = b
            if fill_value: a[np.ix_(fill_mask,fill_mask)] = fill_value
            return a
        elif axis == 0:
            a = np.zeros((n,b.shape[1]), dtype=b.dtype)
            a[pass_mask,:] = b
            if fill_value: a[fill_mask,:] = fill_value
            return a
        elif axis == 1:
            a = np.zeros((b.shape[0],n), dtype=b.dtype)
            a[:,pass_mask] = b
            if fill_value: a[:,fill_mask] = fill_value
            return a
        else:
            raise TypeError("axis must be 0, 1 or (0,1)")
    raise TypeError("Input array must be rank 1 or 2")


def mask_normalize(a, pass_mask, axis=0):
    a = np.array(a, dtype=a.dtype)
    if axis == 0:
        s = a[pass_mask,:].sum(axis=0)[pass_mask]
        a[:,pass_mask] /= s
    else:
        s = a[:,pass_mask].sum(axis=1)[pass_mask]
        a[pass_mask,:] /= s        
    return a


def mask_compress_starts(starts, pass_mask):
    # if a start position is in a masked region, attempt to shift it to the 
    # first unmasked position in the interval
    keep_pos = where(pass_mask)
    boundaries = np.r_[starts, len(pass_mask)]
    relocated_starts = []
    for i, curr in enumerate(boundaries[:-1]):
        next = boundaries[i+1]
        j = curr
        while j < next:
            if j in keep_pos:
                relocated_starts.append(j)
                break
            j += 1
    relocated_starts = np.array(relocated_starts)
    starts_mask = np.zeros(len(pass_mask), dtype=bool)
    starts_mask[relocated_starts] = True
    new_starts = where(starts_mask[pass_mask])
    return new_starts


def mask_restore_starts(starts, pass_mask):
    # Convert compressed coordinates to original coordinates
    included = where(pass_mask)
    new_starts = included[starts]
    # Properly terminate intervals that end just before a masked region begins.
    # This creates an interval that spans the masked region.
    breaks = np.diff(included) > 1
    breaks_left = included[:-1][breaks]
    breaks_right = included[1:][breaks]
    new_starts = list(new_starts) + \
        [breaks_left[i] for (i, r) in enumerate(breaks_right) if r in new_starts]
    # Start a new interval if the end of the sequence is masked.
    # This creates a terminal interval in the final masked region.
    if pass_mask[-1] == False:
        j = len(pass_mask) - 1
        while j > -1:
            if pass_mask[j]:
                new_starts.append(j+1)
                break
            j -= 1
    new_starts.sort()
    return new_starts


def cmap2mmap(cmap):
    mmap = [where(cmap == c) for c in set(cmap)]
    mmap.sort(key=len, reverse=True)
    return mmap


def mmap2cmap(N, mmap):
    cmap = -np.ones(N, dtype=int)
    for c, members in mmap:
        for m in members:
            cmap[m] = c
    return cmap


def spans2cmap(N, spans):
    cmap = -np.ones(N, dtype=int)
    for i, (a,b) in enumerate(sorted(spans)):
        cmap[a:b] = i
    return cmap


def cmap2spans(cmap):
    mdict = defaultdict(list)
    for i, c in enumerate(cmap):
        mdict[c].append(i)
    spans = []
    for c, members in mdict.items():
        assert np.all(np.diff(sorted(members)) == 1)
        spans.append( (min(members), max(members)+1) )
    return spans
