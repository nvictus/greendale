# Greendale

Community finding tools.

There are two representations of community assignments. The representations assume that both nodes and communities are labelled using non-negative integers starting at zero. Unassigned nodes get a community label of -1.

1\. **community map**: 1D numpy array that maps nodes to their community label

```python
cmap = np.array([0,1,0,0,1,1])
```

2\. **members map**: list of 1D numpy arrays that maps community labels to their member nodes

```python
mmap = [
	np.array([0,2,3]), 
	np.array([1,4,5]),
]
```

Segmentations can additionally be represented as a list of tuples containing interval extents (spans). The endpoints are given using the convention of integer ranges in Python: zero-based start, one-based end.

```python
spans = [ 
	(0, 5),
	(5, 13),
	(13, 20),
]
```

